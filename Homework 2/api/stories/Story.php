<?php

require_once "../Database.php";

class Story
{
    private $conn;
    private $db_name = "storymanager";
    private $table_name = "stories";

    public $id;
    public $writerId;
    public $title;
    public $description;
//    private $createdAt;
//    private $updatedAt;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function read()
    {
        $sql = "SELECT * FROM storymanager.stories LEFT JOIN storymanager.writers on stories.writer_id = writers.id";
        $stmt = $this->conn->query($sql);
        return $stmt;
    }

    public function create()
    {
        $sql = "INSERT INTO storymanager.stories(writer_id, title, description) VALUES(:writer_id, :title, :description);";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":writer_id", $this->writerId);
        $stmt->bindParam(":title", $this->title);
        $stmt->bindParam(":description", $this->description);

        $stmt->execute();
    }

    public function delete()
    {
        $sql = "DELETE FROM storymanager.stories WHERE id = :id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":id", $this->id);

        $stmt->execute();
    }

    public function update()
    {
        $sql = "UPDATE storymanager.stories SET writer_id = :writer_id, title = :title, description = :description WHERE id = :id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":id", $this->id);
        $stmt->bindParam(":writer_id", $this->writerId);
        $stmt->bindParam(":title", $this->title);
        $stmt->bindParam(":description", $this->description);

        $stmt->execute();
    }
}
