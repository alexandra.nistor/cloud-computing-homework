<?php
header("Access-Control-Origin: *");
header("Content-Type: application/json, charset = UTF-8");
header("Access-Control-Allow-Methods: POST");

require_once "../Database.php";
require_once "./Story.php";

$database = new Database();
$conn = $database->getConnection();

$story = new Story($conn);
$data = json_decode(file_get_contents("php://input"));
$writerId = $data->writer_id;
$title = $data->title;
$description = $data->description;

$error = "";

// checks if all fields are completed
function testNull($writerId, $title, $description) {
    if (!empty($writerId) && !empty($title) && !empty($description) ) {
        return true;
    }
    $GLOBALS["error"] = "Incomplete data";
    return false;
}

// checks if writer exists
function testWriter($writerId, $conn) {
    $sql = "SELECT * FROM storymanager.writers WHERE id = '$writerId'";
    $result = $conn->query($sql);
    $writerIdResult = $result->fetchColumn();

    if($writerIdResult != null) {
        return true;
    }
    $GLOBALS["error"] = "Writer not found";
    return false;
}

if($_SERVER["REQUEST_METHOD"] === "POST") {
    if (testNull($writerId, $title, $description) &&
        testWriter($writerId, $conn)
    ) {
        $story->writerId = $writerId;
        $story->title = $title;
        $story->description = $description;

        $story->create();
        http_response_code(201);
        echo json_encode(
            array("message" => "Story created")
        );
    }
    else {
        http_response_code(400);
        echo json_encode(
            array("message" => $error)
        );
    }
}
else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Internal server error")
    );
}
