<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset = UTF-8");
header("Access-Control-Allow-Methods: GET");

require_once "./Story.php";
require_once "../Database.php";

$database = new Database();
$conn = $database->getConnection();

$story = new Story($conn);
$stmt = $story->read();

$stories_arr = array();

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    if ($row["writer_id"] != null) {
        $writer = array (
            "id" => $row["writer_id"],
            "first_name" => $row["first_name"],
            "last_name" => $row["last_name"],
            "email" => $row["email"],
        );
    }
    else {
        $writer = null;
    }
    $story_item = array(
        "id" => $row["id"],
        "title" => $row["title"],
        "description" => $row["description"],
        "writer" => $writer
    );
    array_push($stories_arr, $story_item);
}

if ($_SERVER["REQUEST_METHOD"] === "GET") {
    http_response_code(200);
    echo json_encode($stories_arr);
}
else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Internal server error")
    );
}
