<?php
header("Access-Control-Origin: *");
header("Content-Type: application/json, charset = UTF-8");
header("Access-Control-Allow-Methods: DELETE");

require_once '../Database.php';
require_once './Story.php';

$database = new Database();
$conn = $database->getConnection();

$story = new Story($conn);
$data = json_decode(file_get_contents("php://input"));
$story->id = isset($_GET["id"]) ? $_GET["id"] : die();

$sql = "SELECT * FROM storymanager.stories WHERE id = '$story->id'";
$result = $conn->query($sql);
$idResult = $result->fetchColumn();

if($_SERVER["REQUEST_METHOD"] === "DELETE") {
    if ($idResult != null) {
        $story->delete();
        http_response_code(200);
        echo json_encode(
            array("message" => "Story deleted")
        );
    }
    else {
        http_response_code(404);
        echo json_encode(
            array("message" => "Story not found")
        );
    }
}
else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Internal server error")
    );
}
