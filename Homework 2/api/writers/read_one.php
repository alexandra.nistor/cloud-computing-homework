<?php
header("Access-Control-Origin: *");
header("Content-Type: application/json; charset = UTF-8");
header("Access-Control-Allow-Methods: GET");

require_once "../Database.php";
require_once "./Writer.php";

$database = new Database();
$conn = $database->getConnection();

$writer = new Writer($conn);
$writer->id = isset($_GET["id"]) ? $_GET["id"] : die();
$writer->readOne();

$writer_arr = array();

if($_SERVER["REQUEST_METHOD"] === "GET") {
    if ($writer->firstName != null) {
        $writer_arr = array(
            'id' => $writer->id,
            'first_name' => $writer->firstName,
            'last_name' => $writer->lastName,
            'email' => $writer->email
        );

        http_response_code(200);
        echo json_encode($writer_arr);
    }
    else {
        http_response_code(404);
        echo json_encode(
            array("message" => "Writer not found")
        );
    }
}
else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Internal server error")
    );
}
