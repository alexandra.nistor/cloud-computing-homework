<?php
header("Access-Control-Origin: *");
header("Content-Type: application/json, charset = UTF-8");
header("Access-Control-Allow-Methods: POST");

require_once "../Database.php";
require_once "./Writer.php";

$database = new Database();
$conn = $database->getConnection();

$writer = new Writer($conn);
$data = json_decode(file_get_contents("php://input"));
$firstName = $data->first_name;
$lastName = $data->last_name;
$email = $data->email;

$error = "";

// checks if all fields are completed
function testNull($firstName, $lastName, $email) {
    if (!empty($firstName) && !empty($lastName) && !empty($email) ) {
        return true;
    }
    $GLOBALS["error"] = "Incomplete data";
    return false;
}

// checks if a name contains only letters and white space
function testName($name) {
    if (preg_match("/^[a-zA-Z ]*$/", $name)) {
        return true;
    }
    $GLOBALS["error"] = "Only letters and white space allowed for names";
    return false;
}

// checks if email has a valid format - contains "@"
function testEmail($email) {
    if (filter_var($email, FILTER_VALIDATE_EMAIL)){
        return true;
    }
    $GLOBALS["error"] = "Invalid email format";
    return false;
}

// checks if email is unique
function testUniqueEmail($email, $conn) {
    $sql = "SELECT * FROM storymanager.writers WHERE email ='$email'";
    $result = $conn->query($sql);
    $emailNumber = $result->fetchColumn();

    if ($emailNumber == 0) {
        return true;
    }
    $GLOBALS["error"] = "Email already in use";
    return false;
}

if($_SERVER["REQUEST_METHOD"] === "POST") {
    if (testNull($firstName, $lastName, $email) &&
        testName($firstName) &&
        testName($lastName) &&
        testEmail($email) &&
        testUniqueEmail($email, $conn)

    ) {
        $writer->firstName = $firstName;
        $writer->lastName = $lastName;
        $writer->email = $email;

        $writer->create();
        http_response_code(201);
        echo json_encode(
            array("message" => "Writer created")
        );
    }
    else {
        http_response_code(400);
        echo json_encode(
            array("message" => $error)
        );
    }
}
else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Internal server error")
    );
}
