<?php

class Writer
{
    private $conn;
    private $db_name = "storymanager";
    private $table_name = "writers";

    public $id;
    public $firstName;
    public $lastName;
    public $email;
//    private $createdAt;
//    private $updatedAt;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function read()
    {
        $sql = "SELECT * FROM storymanager.writers";
        $stmt = $this->conn->query($sql);
        return $stmt;
    }

    public function readOne()
    {
        $sql = "SELECT * FROM storymanager.writers where id = :id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":id", $this->id);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->firstName = $row["first_name"];
        $this->lastName = $row["last_name"];
        $this->email = $row["email"];
    }

    public function create()
    {
        $sql = "INSERT INTO storymanager.writers(first_name, last_name, email) VALUES(:first_name, :last_name, :email)";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":first_name", $this->firstName);
        $stmt->bindParam(":last_name", $this->lastName);
        $stmt->bindParam(":email", $this->email);
        $stmt->execute();
    }

    public function update()
    {
        $sql = "UPDATE storymanager.writers SET first_name = :first_name, last_name = :last_name, email = :email WHERE id = :id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":id", $this->id);
        $stmt->bindParam(":first_name", $this->firstName);
        $stmt->bindParam(":last_name", $this->lastName);
        $stmt->bindParam(":email", $this->email);
        $stmt->execute();
    }

    public function delete()
    {
        $sql = "DELETE FROM storymanager.writers WHERE id = :id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":id", $this->id);
        $stmt->execute();
    }
}
