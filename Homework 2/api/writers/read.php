<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset = UTF-8");
header("Access-Control-Allow-Methods: GET");

require_once "./Writer.php";
require_once "../Database.php";

$database = new Database();
$conn = $database->getConnection();

$writer = new Writer($conn);
$stmt = $writer->read();

$writers_arr = array();

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $writer_item = array(
        "id" => $row["id"],
        "first_name" => $row["first_name"],
        "last_name" => $row["last_name"],
        "email" => $row["email"],

    );
    array_push($writers_arr, $writer_item);
}

if ($_SERVER["REQUEST_METHOD"] === "GET") {
    http_response_code(200);
    echo json_encode($writers_arr);
}
else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Internal server error")
    );
}
