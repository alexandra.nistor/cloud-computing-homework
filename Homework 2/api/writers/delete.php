<?php
header("Access-Control-Origin: *");
header("Content-Type: application/json, charset = UTF-8");
header("Access-Control-Allow-Methods: DELETE");

require_once '../Database.php';
require_once './Writer.php';

$database = new Database();
$conn = $database->getConnection();

$writer = new Writer($conn);
$data = json_decode(file_get_contents("php://input"));
$writer->id = isset($_GET["id"]) ? $_GET["id"] : die();

$sql = "SELECT * FROM storymanager.writers WHERE id = '$writer->id'";
$result = $conn->query($sql);
$idResult = $result->fetchColumn();

if($_SERVER["REQUEST_METHOD"] === "DELETE") {
    if ($idResult != null) {
        $writer->delete();
        http_response_code(200);
        echo json_encode(
            array("message" => "Writer deleted")
        );
    }
    else {
        http_response_code(404);
        echo json_encode(
            array("message" => "Writer not found")
        );
    }
}
else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Internal server error")
    );
}
